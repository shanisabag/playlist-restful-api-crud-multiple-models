import { NextFunction, Request, Response } from "express";
import HttpException from "../exceptions/http.exception.js";
import UrlNotFoundException from "../exceptions/urlNotFound.exception.js";

// url not found
export const urlNotFound = (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    next(new UrlNotFoundException(req.path));
};

// response with error
export const responseWithError = (
    err: HttpException | UrlNotFoundException,
    req: Request,
    res: Response,
    next: NextFunction
) => {
    const response = {
        status: err.status || 500,
        message: err.message || "something went wrong",
        stack: err.stack,
    };
    res.status(response.status).send({ response });
};
