// import Promise from "bluebird";
// import { ObjectId } from "mongoose";
// import { Iobj, ISong } from "../typing";
// import artist_model from "../modules/artist/artist.model.js";
// import playlist_model from "../modules/playlist/playlist.model.js";
// import song_model from "../modules/song/song.model.js";
// import { delete_song_by_id } from "../modules/song/song.service.js";

// export const add_song_to_artist = async (
//     artist_id: string,
//     song: ISong,
//     song_id: string
// ) => {
//     const artist = await artist_model.findById(artist_id);
//     if (!artist.songs.includes(song_id)) {
//         artist.songs.push(song);
//         await artist.save();
//     }
// };

// // remove song from all playlists' songs
// export const remove_song_from_playlists = async (song_id: string) => {
//     const playlists = await playlist_model.find({ "songs._id": song_id });
//     Promise.each(playlists, async (playlist) => {
//         // remove song from playlist's songs
//         const updated_songs = playlist.songs.filter(
//             (song: Iobj) => song.id !== song_id
//         );
//         playlist.songs = updated_songs;
//         await playlist.save();
//     });
// };

// // delete all artist's songs ( + from the playlists )
// export const remove_artist_songs = async (artist_id: string) => {
//     const playlists = await playlist_model.find({
//         "songs.composer": artist_id,
//     });
//     const songs_to_remove: string[] = [];
//     Promise.each(playlists, async (playlist) => {
//         // remove artist's song from playlist's songs
//         const updated_songs = playlist.songs.filter((song: Iobj) => {
//             songs_to_remove.push(song.id.toString());
//             return song.composer.toString() !== artist_id;
//         });
//         playlist.songs = updated_songs;
//         await playlist.save();
//     });
//     // remove the artist's songs
//     for (const song_id of songs_to_remove) {
//         await delete_song_by_id(song_id);
//     }
// };

// // remove playlist from all songs' playlists
// export const remove_playlist_from_songs = async (playlist_id: string) => {
//     const songs = await song_model.find();
//     Promise.each(songs, async (song) => {
//         if (song.playlists.includes(playlist_id)) {
//             const updated_playlists = song.playlists.filter(
//                 (id: ObjectId) => id.toString() !== playlist_id
//             );
//             song.playlists = updated_playlists;
//             await song.save();
//         }
//     });
// };
