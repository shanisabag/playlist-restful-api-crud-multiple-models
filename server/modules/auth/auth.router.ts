import express, { NextFunction, Request, Response } from "express";
import jwt from "jsonwebtoken";
import ms from "ms";
import cookieParser from "cookie-parser";
import bcrypt from "bcrypt";
import { add_user_validation } from "../user/user.validation.js";
import user_model from "../user/user.model.js";
import HttpException from "../../exceptions/http.exception.js";
import raw from "../../middleware/route.async.wrapper.js";

const router = express.Router();
router.use(cookieParser());

const { APP_SECRET, ACCESS_TOKEN_EXPIRATION, REFRESH_TOKEN_EXPIRATION } =
    process.env;

// verify auth
export const verify_auth = raw(
    async (req: Request, res: Response, next: NextFunction) => {
        const access_token = req.headers["x-access-token"];
        if (!access_token) throw new HttpException(403, "No token provided.");

        // verifies secret and checks exp
        const decoded = await jwt.verify(
            access_token as string,
            APP_SECRET as string
        );

        // if everything is good, save to request for use in other routes
        req.user_id = (decoded as jwt.JwtPayload).id;
        next();
    }
);

router.get("/get-access-token", async (req, res) => {
    //get refresh_token from client - req.cookies
    const { refresh_token } = req.cookies;

    console.log({ refresh_token });

    if (!refresh_token)
        throw new HttpException(
            403,
            "No refresh_token provided. Please login."
        );

    try {
        // verifies secret and checks expiration
        const decoded = await jwt.verify(refresh_token, APP_SECRET as string);
        console.log({ decoded });

        //check user refresh token in DB
        const { id, profile } = decoded as jwt.JwtPayload;

        const access_token = jwt.sign(
            { id, some: "other value" },
            APP_SECRET as string,
            {
                expiresIn: ACCESS_TOKEN_EXPIRATION, //expires in 1 minute
            }
        );

        res.status(200).json(
            `access token: ${access_token} profile: ${profile}`
        );
    } catch (error) {
        throw new HttpException(
            401,
            "Unauthorized - Failed to verify refresh_token."
        );
    }
});

// register
router.post(
    "/register",
    raw(async (req, res) => {
        await add_user_validation(req.body);

        const { first_name, last_name, email, password } = req.body;

        // check if email exists
        const exists = await user_model.findOne({ email: email });
        if (exists) {
            throw new HttpException(
                409,
                "Email already exists. Please try again"
            );
        }

        // hash password
        const hash_pwd = await bcrypt.hash(password, 7);

        // add user to db
        const user = await user_model.create({
            first_name,
            last_name,
            email,
            password: hash_pwd,
            playlists: [],
            refresh_token: "",
        });
        res.status(200).json(user);
    })
);

// login
router.post(
    "/login",
    raw(async (req, res) => {
        const { email, password } = req.body;
        const user = await user_model.findOne({ email: email });
        if (!user) throw new HttpException(403, "wrong email or password");

        const same_pwd = await bcrypt.compare(password, user.password);
        if (same_pwd) {
            const user_id = user.id.toString();

            // create an access_token
            const access_token = jwt.sign(
                { id: user_id, some: "other value" },
                APP_SECRET as string,
                {
                    expiresIn: ACCESS_TOKEN_EXPIRATION, // expires in 1 minute
                }
            );
            // create a refresh_token
            const refresh_token = jwt.sign(
                { id: user_id, profile: JSON.stringify(user) },
                APP_SECRET as string,
                {
                    expiresIn: REFRESH_TOKEN_EXPIRATION, // expires in 60 days... long-term...
                }
            );

            // save refresh per user in DB
            const payload = {
                refresh_token: refresh_token,
            };

            const update_user = await user_model.findByIdAndUpdate(
                user_id,
                payload,
                {
                    new: true,
                    upsert: true,
                }
            );

            res.cookie("refresh_token", refresh_token, {
                maxAge: ms("60d"), //60 days
                httpOnly: true,
            });

            // response with access token ****
            res.status(200).json(`access token: ${access_token}`);
        } else {
            throw new HttpException(403, "wrong email or password");
        }
    })
);

// logout
router.get(
    "/logout",
    verify_auth,
    raw(async (req, res) => {
        // clear refresh_token from user in DB
        const user_id = req.user_id;

        const payload = {
            refresh_token: "",
        };

        const update_user = await user_model.findByIdAndUpdate(
            user_id,
            payload,
            {
                new: true,
                upsert: true,
            }
        );

        res.clearCookie("refresh_token");
        res.status(200).json({ status: "You are logged out" });
    })
);

export default router;
