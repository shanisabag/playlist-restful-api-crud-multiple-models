import express from "express";
import raw from "../../middleware/route.async.wrapper.js";
import {
    // add_song_to_artist,
    create_artist,
    delete_artist_by_id,
    get_all_artists,
    get_artist_by_id,
    update_artist_by_id,
} from "./artist.controller.js";

const router = express.Router();

// get all artists
router.get("/", raw(get_all_artists));

// create a new artist
router.post("/", raw(create_artist));

// no need
// add a song to an artist
// router.post("/:artist_id/song/:song_id", raw(add_song_to_artist));

// get an artist by id
router.get("/:artist_id", raw(get_artist_by_id));

// update an artist by id
router.patch("/:artist_id", raw(update_artist_by_id));

// delete an artist by id
router.delete("/:artist_id", raw(delete_artist_by_id));

export default router;
