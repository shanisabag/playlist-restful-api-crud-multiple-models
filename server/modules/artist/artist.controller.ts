import { Request, Response } from "express";
import UrlNotFoundException from "../../exceptions/urlNotFound.exception.js";
// import { remove_artist_songs } from "../../utils/utils.js";

import * as artist_service from "./artist.service.js";
import * as playlist_service from "../playlist/playlist.service.js";
import {
    add_artist_validation,
    update_artist_validation,
} from "./artist.validation.js";

export const get_all_artists = async (req: Request, res: Response) => {
    const artists = await artist_service.get_all_artists();
    res.status(200).json(artists);
};

export const create_artist = async (req: Request, res: Response) => {
    await add_artist_validation(req.body);
    const artist = await artist_service.create_artist(req.body);
    res.status(200).json(artist);
};

// no need
// export const add_song_to_artist = async (req: Request, res: Response) => {
//     const { artist_id, song_id } = req.params;
//     const artist = await artist_service.add_song_to_artist(artist_id, song_id);
//     res.status(200).json(artist);
// };

export const get_artist_by_id = async (req: Request, res: Response) => {
    const artist = await artist_service.get_artist_by_id(req.params.artist_id);
    if (!artist) throw new UrlNotFoundException(`/api/artists${req.path}`);
    res.status(200).json(artist);
};

export const update_artist_by_id = async (req: Request, res: Response) => {
    await update_artist_validation(req.body);
    const artist = await artist_service.update_artist_by_id(
        req.params.artist_id,
        req.body
    );
    res.status(200).json(artist);
};

export const delete_artist_by_id = async (req: Request, res: Response) => {
    const artist = await artist_service.delete_artist_by_id(
        req.params.artist_id
    );
    if (!artist) throw new UrlNotFoundException(`/api/artists${req.path}`);
    // delete all artist's songs ( + from the playlists )
    await playlist_service.remove_artist_songs(req.params.artist_id);
    res.status(200).json(artist);
};
