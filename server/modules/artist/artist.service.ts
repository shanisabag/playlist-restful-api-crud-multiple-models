import { IArtist, Iobj, ISong } from "../../typing.js";
// import { remove_artist_songs } from "../../utils/utils.js";
import artist_model from "./artist.model.js";
// import song_model from "../song/song.model.js";

export const get_all_artists = async () => {
    const artists = await artist_model.find().populate("songs");
    return artists;
};

export const create_artist = async (payload: IArtist) => {
    const artist = await artist_model.create(payload);
    return artist;
};

// no need
// export const add_song_to_artist = async (
//     artist_id: string,
//     song_id: string
// ) => {
//     const artist = await artist_model.findById(artist_id);
//     const song = await song_model.findById(song_id);
//     if (!artist.songs.includes(song_id)) {
//         artist.songs.push(song);
//         await artist.save();
//     }
//     return artist;
// };

export const get_artist_by_id = async (artist_id: string) => {
    const artist = await artist_model.findById(artist_id).populate("songs");
    return artist;
};

export const update_artist_by_id = async (artist_id: string, payload: Iobj) => {
    const artist = await artist_model.findByIdAndUpdate(artist_id, payload, {
        new: true,
        upsert: true,
    });
    return artist;
};

export const delete_artist_by_id = async (artist_id: string) => {
    const artist = await artist_model.findByIdAndDelete(artist_id);
    return artist;
};

export const add_song_to_artist = async (
    artist_id: string,
    song: ISong,
    song_id: string
) => {
    const artist = await artist_model.findById(artist_id);
    if (!artist.songs.includes(song_id)) {
        artist.songs.push(song);
        await artist.save();
    }
};
