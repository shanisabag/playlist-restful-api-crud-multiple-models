import mongoose from "mongoose";
const { Schema, model } = mongoose;

export const artist_schema = new Schema(
    {
        first_name: { type: String, required: true },
        last_name: { type: String, required: true },
        songs: [{ type: Schema.Types.ObjectId, ref: "song", required: true }],
    },
    { timestamps: true }
);

export default model("artist", artist_schema);
