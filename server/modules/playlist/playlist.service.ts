import Promise from "bluebird";
import { Iobj, IPlaylist } from "../../typing.js";
import playlist_model from "./playlist.model.js";
import song_model from "../song/song.model.js";
import user_model from "../user/user.model.js";
import { delete_song_by_id } from "../song/song.service.js";
import HttpException from "../../exceptions/http.exception.js";

export const get_all_playlists = async () => {
    const playlists = await playlist_model.find();
    return playlists;
};

export const create_playlist = async (payload: IPlaylist, user_id: string) => {
    const user = await user_model.findById(user_id);

    const playlist = await playlist_model.create({
        ...payload,
        owner: user,
    });

    // add playlist to user's playlists
    user?.playlists.push(playlist);
    await user?.save();
    return playlist;
};

export const add_song_to_playlist = async (
    playlist_id: string,
    song_id: string,
    user_id: string
) => {
    const playlist = await playlist_model.findById(playlist_id);

    // check if the playlist's owner is the logged in user
    if (playlist.owner.toString() !== user_id)
        throw new HttpException(403, "You don't have permission.");

    const song = await song_model.findById(song_id);
    // add song to playlist's songs
    if (!playlist.songs.includes(song_id)) {
        playlist.songs.push(song);
        await playlist.save();
    }
    // add playlist to song's playlists
    if (!song.playlists.includes(playlist_id)) {
        song.playlists.push(playlist);
        await song.save();
    }
    return playlist;
};

export const remove_song_from_playlist = async (
    playlist_id: string,
    song_id: string,
    user_id: string
) => {
    const playlist = await playlist_model.findById(playlist_id);

    // check if the playlist's owner is the logged in user
    if (playlist.owner.toString() !== user_id)
        throw new HttpException(403, "You don't have permission.");

    const song = await song_model.findById(song_id);

    // remove song from playlist's songs
    const updated_songs = playlist.songs.filter(
        (song: Iobj) => song.id !== song_id
    );
    playlist.songs = updated_songs;
    await playlist.save();

    // remove playlist from song's playlists
    const updated_playlists = song.playlists.filter(
        (id: object) => id.toString() !== playlist_id
    );
    song.playlists = updated_playlists;
    await song.save();

    return playlist;
};

export const get_playlist_by_id = async (playlist_id: string) => {
    const playlist = await playlist_model.findById(playlist_id);
    return playlist;
};

export const update_playlist_by_id = async (
    playlist_id: string,
    payload: Iobj,
    user_id: string
) => {
    const playlist_ = await playlist_model.findById(playlist_id);
    // check if the playlist's owner is the logged in user
    if (playlist_.owner.toString() !== user_id)
        throw new HttpException(403, "You don't have permission.");

    const playlist = await playlist_model.findByIdAndUpdate(
        playlist_id,
        payload,
        {
            new: true,
            upsert: true,
        }
    );
    return playlist;
};

export const delete_playlist_by_id = async (
    playlist_id: string,
    user_id: string
) => {
    const playlist_ = await playlist_model.findById(playlist_id);
    // check if the playlist's owner is the logged in user
    if (playlist_.owner.toString() !== user_id)
        throw new HttpException(403, "You don't have permission.");

    const playlist = await playlist_model.findByIdAndDelete(playlist_id);
    return playlist;
};

// remove song from all playlists' songs
export const remove_song_from_playlists = async (song_id: string) => {
    const playlists = await playlist_model.find({ "songs._id": song_id });
    Promise.each(playlists, async (playlist) => {
        // remove song from playlist's songs
        const updated_songs = playlist.songs.filter(
            (song: Iobj) => song.id !== song_id
        );
        playlist.songs = updated_songs;
        await playlist.save();
    });
};

// delete all artist's songs ( + from the playlists )
export const remove_artist_songs = async (artist_id: string) => {
    const playlists = await playlist_model.find({
        "songs.composer": artist_id,
    });
    const songs_to_remove = await song_model.find({ composer: artist_id });

    Promise.each(playlists, async (playlist) => {
        // remove artist's song from playlist's songs
        const updated_songs = playlist.songs.filter((song: Iobj) => {
            return song.composer.toString() !== artist_id;
        });
        playlist.songs = updated_songs;
        await playlist.save();
    });
    // remove the artist's songs
    Promise.each(songs_to_remove, async (song) => {
        await delete_song_by_id(song.id.toString());
    });
};
