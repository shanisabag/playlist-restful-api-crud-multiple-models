import express from "express";
import raw from "../../middleware/route.async.wrapper.js";
import { verify_auth } from "../auth/auth.router.js";
import {
    add_song_to_playlist,
    create_playlist,
    delete_playlist_by_id,
    get_all_playlists,
    get_playlist_by_id,
    remove_song_from_playlist,
    update_playlist_by_id,
} from "./playlist.controller.js";

const router = express.Router();

// get all playlists
router.get("/", raw(get_all_playlists));

// create a new playlist
router.post("/", verify_auth, raw(create_playlist));

// add a song to a playlist
router.post(
    "/:playlist_id/add/song/:song_id",
    verify_auth,
    raw(add_song_to_playlist)
);

// remove a song from a playlist
router.post(
    "/:playlist_id/remove/song/:song_id",
    verify_auth,
    raw(remove_song_from_playlist)
);

// get a playlist by id
router.get("/:playlist_id", raw(get_playlist_by_id));

// update a playlist by id
router.patch("/:playlist_id", verify_auth, raw(update_playlist_by_id));

// delete a playlist by id
router.delete("/:playlist_id", verify_auth, raw(delete_playlist_by_id));

export default router;
