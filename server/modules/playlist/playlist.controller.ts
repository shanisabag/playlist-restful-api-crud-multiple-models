import { Request, Response } from "express";
import UrlNotFoundException from "../../exceptions/urlNotFound.exception.js";

import * as playlist_service from "./playlist.service.js";
import * as song_service from "../song/song.service.js";
import {
    add_playlist_validation,
    update_playlist_validation,
} from "./playlist.validation.js";
import HttpException from "../../exceptions/http.exception.js";

export const get_all_playlists = async (req: Request, res: Response) => {
    const playlists = await playlist_service.get_all_playlists();
    res.status(200).json(playlists);
};

export const create_playlist = async (req: Request, res: Response) => {
    await add_playlist_validation(req.body);
    const playlist = await playlist_service.create_playlist(
        req.body,
        req.user_id
    );
    res.status(200).json(playlist);
};

export const add_song_to_playlist = async (req: Request, res: Response) => {
    const { playlist_id, song_id } = req.params;
    const song = await song_service.get_song_by_id(song_id);
    if (song === null) {
        throw new HttpException(400, "Song does not exist");
    }
    const playlist = await playlist_service.add_song_to_playlist(
        playlist_id,
        song_id,
        req.user_id
    );
    res.status(200).json(playlist);
};

export const remove_song_from_playlist = async (
    req: Request,
    res: Response
) => {
    const { playlist_id, song_id } = req.params;
    const playlist = await playlist_service.remove_song_from_playlist(
        playlist_id,
        song_id,
        req.user_id
    );
    res.status(200).json(playlist);
};

export const get_playlist_by_id = async (req: Request, res: Response) => {
    const playlist = await playlist_service.get_playlist_by_id(
        req.params.playlist_id
    );
    if (!playlist) throw new UrlNotFoundException(`/api/playlists${req.path}`);
    res.status(200).json(playlist);
};

export const update_playlist_by_id = async (req: Request, res: Response) => {
    await update_playlist_validation(req.body);
    const playlist = await playlist_service.update_playlist_by_id(
        req.params.playlist_id,
        req.body,
        req.user_id
    );
    res.status(200).json(playlist);
};

export const delete_playlist_by_id = async (req: Request, res: Response) => {
    const playlist = await playlist_service.delete_playlist_by_id(
        req.params.playlist_id,
        req.user_id
    );
    if (!playlist) throw new UrlNotFoundException(`/api/playlists${req.path}`);
    // delete the playlist from songs' playlists
    await song_service.remove_playlist_from_songs(req.params.playlist_id);
    res.status(200).json(playlist);
};
