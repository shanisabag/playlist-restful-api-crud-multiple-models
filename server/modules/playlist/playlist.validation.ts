import { Iobj, IPlaylist } from "../../typing.js";
import {
    add_playlist_schema,
    update_playlist_schema,
} from "./playlist.scheme.js";

export const add_playlist_validation = async (obj: IPlaylist) => {
    await add_playlist_schema.validateAsync(obj);
};

export const update_playlist_validation = async (obj: Iobj) => {
    await update_playlist_schema.validateAsync(obj);
};
