import mongoose from "mongoose";
import { song_schema } from "../song/song.model.js";
const { Schema, model } = mongoose;

export const playlist_schema = new Schema(
    {
        name: { type: String, required: true },
        songs: [song_schema],
        owner: { type: Schema.Types.ObjectId, ref: "user", required: true },
    },
    { timestamps: true }
);

export default model("playlist", playlist_schema);
