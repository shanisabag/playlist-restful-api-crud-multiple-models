import Joi from "joi";

export const add_playlist_schema = Joi.object({
    name: Joi.string().min(2).max(10).required(),
    songs: Joi.array().length(0).required(),
});

export const update_playlist_schema = Joi.object({
    name: Joi.string().min(2).max(10),
});
