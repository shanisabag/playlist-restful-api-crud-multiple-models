import { Request, Response } from "express";
import * as user_service from "./user.service.js";
import { update_user_validation } from "./user.validation.js";

export const get_user_data = async (req: Request, res: Response) => {
    const user = await user_service.get_user_data(req.user_id);
    res.status(200).json(user);
};

export const update_user = async (req: Request, res: Response) => {
    await update_user_validation(req.body);
    const user = await user_service.update_user(req.body, req.user_id);
    res.status(200).json(user);
};
