import { Iobj } from "../../typing.js";
import user_model from "./user.model.js";

export const get_user_data = async (user_id: string) => {
    const user = await user_model
        .findById(user_id)
        .populate("playlists")
        .select("-_id -password -refresh_token");
    return user;
};

export const update_user = async (payload: Iobj, user_id: string) => {
    const user = await user_model.findByIdAndUpdate(user_id, payload, {
        new: true,
        upsert: true,
    });
    return user;
};
