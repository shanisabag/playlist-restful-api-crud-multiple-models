import Joi from "joi";

export const add_user_schema = Joi.object({
    first_name: Joi.string().min(2).max(10).required(),
    last_name: Joi.string().min(2).max(10).required(),
    email: Joi.string()
        .email({ minDomainSegments: 2, tlds: { allow: ["com", "net"] } })
        .required(),
    password: Joi.string().min(6).required(),
});

export const update_user_schema = Joi.object({
    first_name: Joi.string().min(2).max(10),
    last_name: Joi.string().min(2).max(10),
});
