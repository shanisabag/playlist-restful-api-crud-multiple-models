import express from "express";
import raw from "../../middleware/route.async.wrapper.js";
import { verify_auth } from "../auth/auth.router.js";
import { get_user_data, update_user } from "./user.controller.js";

const router = express.Router();

// get user's data
router.get("/", verify_auth, raw(get_user_data));

// update user
router.patch("/", verify_auth, raw(update_user));

export default router;
