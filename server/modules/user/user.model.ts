import mongoose from "mongoose";
import { IUser } from "../../typing";
const { Schema, model } = mongoose;

export const user_schema = new Schema<IUser>(
    {
        first_name: { type: String, required: true },
        last_name: { type: String, required: true },
        email: { type: String, required: true },
        password: { type: String, required: true },
        playlists: [
            { type: Schema.Types.ObjectId, ref: "playlist", required: true },
        ],
        refresh_token: { type: String },
    },
    { timestamps: true }
);

export default model("user", user_schema);
