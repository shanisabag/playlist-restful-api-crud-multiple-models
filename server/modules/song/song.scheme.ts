import Joi from "joi";

export const add_song_schema = Joi.object({
    name: Joi.string().min(2).max(10).required(),
    composer: Joi.string().length(24).required(),
    playlists: Joi.array().length(0).required(),
});

export const update_song_schema = Joi.object({
    name: Joi.string().min(2).max(10),
});
