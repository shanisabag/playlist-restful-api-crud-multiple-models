import Promise from "bluebird";
import { ObjectId } from "mongoose";
import { Iobj, ISong } from "../../typing.js";
import song_model from "./song.model.js";

export const get_all_songs = async () => {
    const songs = await song_model.find().populate("composer");
    return songs;
};

export const create_song = async (payload: ISong) => {
    const song = await song_model.create(payload);
    return song;
};

export const get_artist_songs = async (artist_id: string) => {
    const songs = await song_model.find({ composer: artist_id });
    return songs;
};

export const get_song_by_id = async (song_id: string) => {
    const song = await song_model.findById(song_id);
    return song;
};

export const update_song_by_id = async (song_id: string, payload: Iobj) => {
    const song = await song_model.findByIdAndUpdate(song_id, payload, {
        new: true,
        upsert: true,
    });
    return song;
};

export const delete_song_by_id = async (song_id: string) => {
    const song = await song_model.findByIdAndDelete(song_id);
    return song;
};

// remove playlist from all songs' playlists
export const remove_playlist_from_songs = async (playlist_id: string) => {
    const songs = await song_model.find();
    Promise.each(songs, async (song) => {
        if (song.playlists.includes(playlist_id)) {
            const updated_playlists = song.playlists.filter(
                (id: ObjectId) => id.toString() !== playlist_id
            );
            song.playlists = updated_playlists;
            await song.save();
        }
    });
};
