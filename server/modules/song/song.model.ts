import mongoose from "mongoose";
const { Schema, model } = mongoose;

export const song_schema = new Schema(
    {
        name: { type: String, required: true },
        composer: {
            type: Schema.Types.ObjectId,
            ref: "artist",
            required: true,
        },
        playlists: [
            { type: Schema.Types.ObjectId, ref: "playlist", required: true },
        ],
    },
    { timestamps: true }
);

export default model("song", song_schema);
