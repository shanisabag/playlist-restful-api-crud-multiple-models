import express from "express";
import raw from "../../middleware/route.async.wrapper.js";
import {
    create_song,
    delete_song_by_id,
    get_all_songs,
    get_artist_songs,
    get_song_by_id,
    update_song_by_id,
} from "./song.controller.js";

const router = express.Router();

// get all songs
router.get("/", raw(get_all_songs));

// create a new song
router.post("/", raw(create_song));

// get all artist's songs
router.get("/artist/:artist_id", raw(get_artist_songs));

// get a song by id
router.get("/:song_id", raw(get_song_by_id));

// update a song by id
router.patch("/:song_id", raw(update_song_by_id));

// delete a song by id
router.delete("/:song_id", raw(delete_song_by_id));

export default router;
