import { Request, Response } from "express";
import UrlNotFoundException from "../../exceptions/urlNotFound.exception.js";
// import {
//     add_song_to_artist,
//     remove_song_from_playlists,
// } from "../../utils/utils.js";
import * as song_service from "./song.service.js";
import * as artist_service from "../artist/artist.service.js";
import * as playlist_service from "../playlist/playlist.service.js";

import {
    add_song_validation,
    update_song_validation,
} from "./song.validation.js";
import HttpException from "../../exceptions/http.exception.js";

export const get_all_songs = async (req: Request, res: Response) => {
    const songs = await song_service.get_all_songs();
    res.status(200).json(songs);
};

export const create_song = async (req: Request, res: Response) => {
    await add_song_validation(req.body);
    // check if composer exsits
    const artist = await artist_service.get_artist_by_id(req.body.composer);
    if (artist === null) {
        throw new HttpException(400, "Composer does not exist");
    }
    const song = await song_service.create_song(req.body);
    // add the song to the artist's songs
    const { composer, id: song_id } = song;
    await artist_service.add_song_to_artist(composer.toString(), song, song_id);
    res.status(200).json(song);
};

export const get_artist_songs = async (req: Request, res: Response) => {
    const songs = await song_service.get_artist_songs(req.params.artist_id);
    res.status(200).json(songs);
};

export const get_song_by_id = async (req: Request, res: Response) => {
    const song = await song_service.get_song_by_id(req.params.song_id);
    if (!song) throw new UrlNotFoundException(`/api/songs${req.path}`);
    res.status(200).json(song);
};

export const update_song_by_id = async (req: Request, res: Response) => {
    await update_song_validation(req.body);
    const song = await song_service.update_song_by_id(
        req.params.song_id,
        req.body
    );
    res.status(200).json(song);
};

export const delete_song_by_id = async (req: Request, res: Response) => {
    const song = await song_service.delete_song_by_id(req.params.song_id);
    if (!song) throw new UrlNotFoundException(`/api/songs${req.path}`);
    // remove the song from all playlists' songs
    const { id: song_id } = song;
    await playlist_service.remove_song_from_playlists(song_id);
    res.status(200).json(song);
};
