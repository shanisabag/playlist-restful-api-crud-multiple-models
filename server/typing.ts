export interface IArtist {
    first_name: string;
    last_name: string;
    songs: string[];
}

export interface ISong {
    name: string;
    composer: string;
    playlists: string[];
}

export interface IPlaylist {
    name: string;
    songs: ISong[];
}

export interface IUser {
    first_name: string;
    last_name: string;
    email: string;
    password: string;
    playlists: IPlaylist[];
    refresh_token: string;
}

export interface Iobj {
    [key: string]: string;
}

declare global {
    // eslint-disable-next-line @typescript-eslint/no-namespace
    namespace Express {
        interface Request {
            user_id: string;
        }
    }
}
