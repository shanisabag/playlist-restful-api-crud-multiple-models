import express from "express";
import morgan from "morgan";
import log from "@ajar/marker";
import cors from "cors";

import { connect_db } from "./db/mongoose.connection.js";

import artist_router from "./modules/artist/artist.router.js";
import song_router from "./modules/song/song.router.js";
import playlist_router from "./modules/playlist/playlist.router.js";
import auth_router from "./modules/auth/auth.router.js";
import user_router from "./modules/user/user.router.js";
import { responseWithError, urlNotFound } from "./middleware/errors.handler.js";
class start_api {
    private _PORT: number;
    private _HOST: string;
    private _DB_URI: string;
    private _app;

    constructor() {
        this._PORT = Number(process.env.PORT) || 3030;
        this._HOST = process.env.HOST || "localhost";
        this._DB_URI =
            process.env.DB_URI ||
            "mongodb://localhost:27017/playlist-restfulapi-crudmultiplemodels";
        this._app = express();
    }

    init() {
        // middleware
        this._app.use(cors());
        this._app.use(morgan("dev"));
        this._app.use(express.json());

        // routing
        this.routing();

        // error handling
        this.errors_handler();

        //start the express api server
        this.connect_to_db().catch(console.log);
    }

    private routing() {
        this._app.use("/api/auth", auth_router);
        this._app.use("/api/users", user_router);
        this._app.use("/api/artists", artist_router);
        this._app.use("/api/songs", song_router);
        this._app.use("/api/playlists", playlist_router);
    }

    private errors_handler() {
        this._app.use("*", urlNotFound);
        this._app.use(responseWithError);
    }

    private async connect_to_db() {
        //connect to mongo db
        await connect_db(this._DB_URI);
        this._app.listen(this._PORT, this._HOST);
        log.magenta(
            "api is live on",
            ` ✨ ⚡  http://${this._HOST}:${this._PORT} ✨ ⚡`
        );
    }
}

const app = new start_api();
app.init();
